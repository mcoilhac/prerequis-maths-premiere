---
author: Mireille Coilhac
title: 👏 Crédits
---

😀 Un grand merci à Frédéric Zinelli, et Vincent-Xavier Jumel qui ont réalisé la partie technique de ce site. 

😀 Un grand merci à Nicolas Revéret qui a codé la représentation des fractions.

😀 Un grand merci à l'association [COOPMATHS](https://coopmaths.fr/www/){:target="_blank" } et en particulier à MathALÉA qui génère les exercices aléatoires. 

Le site est hébergé par la forge des communs numériques éducatifs <a href="https://docs.forge.apps.education.fr/">
<span aria-label="Avatar" aria-hidden="true" data-type="round" data-color="3" class="_avatar_k41ul_17 mx_BaseAvatar" style="--cpd-avatar-size: 16px;"><img loading="lazy" alt="" src="https://matrix.agent.education.tchap.gouv.fr/_matrix/media/v3/thumbnail/matrix.agent.education.tchap.gouv.fr/de0e2fe63b40dd452178360baa3ff29ba16d8b98?width=16&amp;height=16&amp;method=crop" crossorigin="anonymous" referrerpolicy="no-referrer" class="_image_k41ul_49" data-type="round" width="16px" height="16px"></span><span class="mx_Pill_text">Centre de documentation</span></a>

![AEIF](../assets/images/logo_aeif_300.png){width=7%}    
Le modèle du site a été créé par l'  [Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/), et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/) pour les QCM (et la possibilité de faire des exercices Python).