---
author: Nicolas Revéret Mireille Coilhac
title: Quelques fractions
tags:
 - calcul
---

!!! info "Des fractions particulières"

    * $\dfrac{2}{2}=1$
    * $\dfrac{2}{1}=2$
    * $\dfrac{1}{2} \neq \dfrac{2}{1}$
    * $2 = \dfrac{2}{1}$
    * $\dfrac{0}{2}=0$


## Se représenter les fractions

!!! info "Pour s'entraîner"

    Dans ce qui suit un rectangle **bleu** comme celui ci-dessous représente **1**.

    ![1](images/un.png){ width=40%; : .center }


    Dans ce qui suit un rectangle **vide** comme celui ci-dessous représente **0**.

    ![0](images/zero.png){ width=40%; : .center }

    Vous pourrez visualiser des fractions dont le résultat est entre 0 compris et 3 compris.

    😊 Commencez par représenter les fractions vues ci-dessus, puis faites vos propres essais.
    
    *Auteur de cette animation : Nicolas Revéret*

    <!--- en local mais pas de MAJ
    <div class="centre">
	<iframe 
	src="../a_telecharger/fractions_NR_v2.html"
	width="1000" height="750" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>
    -->

    <div class="centre">
	<iframe 
	src=https://nreveret.forge.apps.education.fr/visualisation_fractions/
	width="1000" height="750" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



## Autres visualisations possibles

!!! info "Pour s'entraîner"

    Vous pouvez déplacer les objets, et tester.

    [Fractions intro](https://phet.colorado.edu/sims/html/fractions-intro/latest/fractions-intro_all.html?locale=fr){ .md-button target="_blank" rel="noopener" }


## QCM : Cocher toutes les bonnes réponses

{{multi_qcm(
["Compléter : $\\dfrac{2}{1}=$", 
["$1$", "$2$", "Je ne sais pas", "$\\dfrac{1}{2}$", "$0,5$"], [2], {"multi": False}],
["Compléter : $\\dfrac{0}{2}=$",
["$1$", "$0$", "Je ne sais pas", "$\\dfrac{0}{1}$", "$\\dfrac{1}{0}$"], [2, 4]],
["Compléter : $\\dfrac{5}{5}=$",
["$1$", "$5$", "Je ne sais pas", "$\\dfrac{5}{1}$", "$\\dfrac{1}{5}$", "0"], [1], {"multi": False}],
["Compléter : $3=$",
["$1$", "$\\dfrac{6}{2}$", "Je ne sais pas", "$\\dfrac{3}{1}$", "$\\dfrac{1}{3}$", "0"], [2, 4]],
multi = True,
    qcm_title = "Testez-vous sur les fractions de base (bouton en bas pour recommencer)",
    DEBUG = False,
    shuffle = True,
)}}
