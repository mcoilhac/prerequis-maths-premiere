---
author: Mireille Coilhac
title: Pourcentages et évolutions
tags:
 - calcul
 - pourcentages
---

## I. Le cours

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y_gDKPidUQ0?si=eJcm2OcDClAn51eW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## II. Exercices

[Pourcentages et évolutions](https://coopmaths.fr/alea/?uuid=d67e9&id=6N33-3&n=5&d=10&cd=1&uuid=b2c55&id=5N110&uuid=12444&id=2S11-2&n=10&d=10&s=4&cd=1&v=eleve&es=011100&title=https://coopmaths.fr/alea/?uuid=d67e9&id=6N33-3&n=5&d=10&cd=1&uuid=b2c55&id=5N110&uuid=12444&id=2S11-2&n=10&d=10&s=4&cd=1&v=eleve&es=011100&title=){ .md-button target="_blank" rel="noopener" }