---
author: Mireille COILHAC
title: Calculatrice graphique
tags:
  - calculatrice
---

!!! info "Il faut savoir"

    * Réaliser unn tableau de valeurs pour une fonction
    * Faire une représentation graphique d'une fonction

## I. Prise en main avec une calculatrice graphique CASIO

???+ question "Premier essai"

    Dans le menu GRAPHE, saisir $y=100 \times x^2 + 50$

    Cliquer sur la touche <kbd>EXE</kbd>

    Que se passe-t-il ?

    ??? success "Solution"

        Il ne s'affiche rien !

        C'est normal, le "fenêtre de vue" par défaut n'est pas appropriée à la représentation graphique de cette fonction.

        ![rien](images/rien.PNG){ width=20% }


!!! info "Chercher un tableau de valeurs"

    * Dans le menu TABLE, régler la plage de valeurs choisie en sélectionnant REGL (ou SET suivant les modèles)
    * Lire les valeurs trouvées pour en déduire une fenêtre de vue adaptée.

!!! info "Régler la fenêtre de vue"

    * Appuyer sur la touche <kbd>SHIFT</kbd> puis V-Window (touche <kbd>F3</kbd>).
    * Régler xmin, xmax, ymin, ymax pour que la courbe représentant la fonction soit visible.


???+ question "Deuxième essai"

    Reprendre la fonction précédante, visualiser le tableau de valeurs pour $x$ entre -5 et 5.

    En déduire une fenêtre de vue, la régler, faire afficher la représentation graphique.

    ??? success "Solution"

        On peut choisir : 
        xmin = -5  
        xmax = 5   
        ymin = -100  
        ymax = 3000  


        grad. correspond aux graduations. Faites un choix judicieux tenant compte des valeurs ! 😂

        On obtient alors : 

        ![rien](images/essai_2.PNG){ width=20% }


    
???+ question "TP papiers" à télécharger"

    [Les fonctions avec Casio](a_telecharger/Utilisation_calculatrice_graphique_Graph_35.pdf){ .md-button target="_blank" rel="noopener" }

## II. Prise en main avec une calculatrice Ti

[Les fonctions avec une Ti 83 +](a_telecharger/200_ti83_fonctions.pdf){ .md-button target="_blank" rel="noopener" }

[Tableau de valeurs avec une Ti 83 Premium](a_telecharger/tableau_valeurs_ti83_premium.pdf){ .md-button target="_blank" rel="noopener" }

[Les fonctions avec une Ti 83 Premium](a_telecharger/fonction_ti83_premium.pdf){ .md-button target="_blank" rel="noopener" }

[Tableau de valeur avec une Ti nspire](a_telecharger/tableau_valeurs_tinspire.pdf){ .md-button target="_blank" rel="noopener" }


## III. Prise en main avec une calculatrice Numworks

[Tableau de valeurs avec Numworks](a_telecharger/tableau_valeurs_numworks.pdf){ .md-button target="_blank" rel="noopener" }

[Les fonctions avec Numworks](a_telecharger/courbe_fonction_numworks.pdf){ .md-button target="_blank" rel="noopener" }
