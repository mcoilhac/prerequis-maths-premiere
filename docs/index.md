---
author: Mireille COILHAC
title: 🏡 Accueil
---

!!! info "Prérequis et automatismes"

    Vous trouverez des prérequis abolument nécessaires pour suivre les mathématiques à partir du début de la première.  
    Il faut acquérir des automatismes très rapides sur ces points de base.


!!! info "S'entraîner à l'infini"

    Dans toutes les feuilles d'exercices, on peut cliquer sur "Nouvel énoncé". De nouveaux exercices, avec leur correction à la demande, sont alors proposés.
    Vous pouvez donc voue entraîner à l'infini 😉 ...

!!! info "S'entraîner **régulièrement** sur papier"

    Les exercices proposés sont à résoudre "sur papier". Le site est là pour vous aider par quelques brefs rappels de cours et des corrections détaillées.

    ✍️ La condition de la réussite est : "faire, refaire et refaire ..." **par écrit**.  
    😊 Il est normal de se tromper. C'est en se trompant qu'on apprend !   
    🤣 Mais ensuite, il faut se corriger !  C'est ce qui permet de mémoriser.


!!! warning "Le bon usage des vidéos"

    Se contenter de regarder une vidéo est assez inefficace pour mémoriser. Il faut se munir d'un papier et d'un crayon, 
    et **refaire par écrit** tout ce qui est proposé sur la vidéo, en la mettant souvent en pause.

!!! info "🌴 Nouveau - encore plus de révisions avec le cahier de vacances"

    Vous trouverez ci-dessous un cahier de vacances qui aborde plus de points, en particulier sur les fonctions. 
    Il permet aussi de travailler en autonomie grâce aux exercices aléatoires et corrections proposées.

    [Cahier de vacances en ligne](https://coopmaths.fr/www/cahiers-de-vacances/1re/){ .md-button target="_blank" rel="noopener" }

    [Cahier de vacances en pdf](https://coopmaths.fr/www/pdf/cdv_premiere.pdf){ .md-button target="_blank" rel="noopener" }

!!! warning "Remarque"

    Ce site est en évolution ...

