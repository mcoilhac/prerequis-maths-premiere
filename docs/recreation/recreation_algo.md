---
author: Mireille COILHAC
title: Récréation
tags:
  - algorithmique
---

## 😺🐶🐔🐬 

Voici un site réalisé par Nicolas Revéret, pour découvrir les bases de l'algorithmique en s'amusant :

[Découverte de l'algorithmique par des énigmes](https://nreveret.forge.apps.education.fr/enigmes/){ .md-button target="_blank" rel="noopener" }



