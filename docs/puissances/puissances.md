---
author: Mireille COILHAC
title: Les puissances
tags:
  - calcul algébrique
---

## I. L'essentiel en vidéos

Bien regarder ces vidéos, très bien expliquées.

### Généralités

<iframe width="560" height="315" src="https://www.youtube.com/embed/mbELqOu9rzU?si=1fJ6q1dpbaE1Kndc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Puissances et produits

<iframe width="560" height="315" src="https://www.youtube.com/embed/EIlnJNzzoK0?si=bWAvnHHvThjW1-U0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Puissances et quotients

<iframe width="560" height="315" src="https://www.youtube.com/embed/P4s-kGoJzik?si=GT7h7ZM7B8b5Uf3u" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## II. Les formules à connaître par coeur

???+ question "Les formules"

    **1.** Formule 1.   
    
    $a^{m} \times a^{n}=$

    ??? success "Solution"

        $a^{m+n}$

    **2.** Formule 2.   
    
    $\dfrac{a^m}{a^n}=$

    ??? success "Solution"

        $a^{m-n}$

    **3.** Formule 3.   
    
    $({a^m})^{n}=$

    ??? success "Solution"

        $a^{m \times n}$

    **4.** Formule 4.   
    
    $a^{-n}=$

    ??? success "Solution"

        $\dfrac{1}{a^n}$

    **5.** Formule 5.   
    
    $(a \times b)^n=$

    ??? success "Solution"

        $a^n \times b^n$

    **6.** Formule 6.   
    
    $(\dfrac{a}{b})^n=$

    ??? success "Solution"

        $(\dfrac{a^n}{b^n})$

    **7.** Formule 7.   
    
    $a^0$

    ??? success "Solution"

        $1$

    **8.** Formule 8.   
    
    $a^1$

    ??? success "Solution"

        $a$

    **9.** Formule 9.   
    
    $\dfrac{1}{a^n}$

    ??? success "Solution"

        $a^{-n}$


## III. Testez-vous :

{{ multi_qcm(
    [
        "Compléter : $2^3=$",
        [
            "9",
            "8",
            "Je ne sais pas",
            "6",
        ],
        [2],
    ],
    [
        "Compléter : $3^2=$",
        [
            "9",
            "8",
            "Je ne sais pas",
            "6",
        ],
        [1],
    ],
    [
        "Compléter : $2^0=$",
        [
            "0",
            "1",
            "Impossible",
            "2",
        ],
        [2],
    ],
    [
        "Compléter : $2^1=$",
        [
            "0",
            "1",
            "Je ne sais pas",
            "2",
        ],
        [4],
    ],
    [
        "Compléter : $0^3=$",
        [
            "0",
            "1",
            "Impossible",
            "3",
        ],
        [1],
    ],
    [
        "Compléter : $16^3 \\times 16^2=$",
        [
            "$16^5$",
            "$16^6$",
            "Je ne sais pas",
            "2",
        ],
        [1],
    ],
    [
        "Compléter : $16^0\\times 16^2=$",
        [
            "$16^0$",
            "$16^2$",
            "Je ne sais pas",
            "16",
            "1",
        ],
        [2],
    ],
    [
        "Compléter : $2^3+2^4=$",
        [
            "$2^7$",
            "$2^{12}$",
            "Je ne sais pas",
            "24",
        ],
        [4],
    ],
    multi = False,
    qcm_title = "QCM à refaire tant que nécessaire ...",
    DEBUG = False,
    shuffle = True,
    ) }}


## IV. Exercices

[Exercices sur les puissances](https://coopmaths.fr/alea/?uuid=6fda8&id=2N31-4&uuid=c71da&id=2N31-3&n=10&d=10&cd=1&uuid=379cd&id=3C10-2&n=10&d=10&s=5&s2=3&cd=1&uuid=b1517&id=can2C03&n=10&d=10&cd=1&uuid=b31eb&id=can2C13&n=10&d=10&cd=1&uuid=8d08f&id=can3C01&n=10&d=10&cd=1&v=eleve&es=011100&title=Exercices+sur+les+puissances){ .md-button target="_blank" rel="noopener" }

