---
author: Mireille COILHAC
title: Systèmes d'équations du premier degré
tags:
  - systemes
---

## Des poules et des lapins

<iframe width="560" height="315" src="https://www.youtube.com/embed/tQ0LAcihpmw?si=juM3Bskj7mlhHGnM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Résoudre un système d'équations du premier degré par substitution

<iframe width="560" height="315" src="https://www.youtube.com/embed/24VsDZK6bN0?si=Y96X21Auz0SiTfwu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Résoudre un système d'équations du premier degré par combinaisons linéaires

<iframe width="560" height="315" src="https://www.youtube.com/embed/rlLlzYMaN-E?si=p_3UxkqkkxuNw75v" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Exercices

[Exercices sur les systèmes d'équations du premier degré](https://coopmaths.fr/alea/?uuid=81fd6&id=2G34-10&n=4&d=10&cd=1&uuid=2eee3&id=2G34-2&uuid=521b6&id=2G34-7&n=3&d=10&s=2&cd=1&uuid=5179b&id=2G34-5&n=5&d=10&s=4&s2=false&s3=false&cd=1&uuid=fade2&id=2G34-8&n=5&d=10&s=3&cd=1&uuid=6fbf9&id=2G34-9&v=eleve&es=021100&title=){ .md-button target="_blank" rel="noopener" }


