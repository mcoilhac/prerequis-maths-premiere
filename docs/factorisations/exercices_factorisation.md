---
author: Mireille COILHAC
title: Factorisations - exercices
tags:
  - calcul algébrique
---

[Exercices variés de factorisations sur MathALÉA](https://coopmaths.fr/alea/?uuid=5f5a6&id=3L11-4&uuid=81fd2&id=3L12&n=5&d=10&s=4&s2=false&cd=1&uuid=c4b73&id=3L12-2&n=10&d=10&s=1&s2=1&s3=1&s4=2&s5=true&cd=1&uuid=9965d&id=3L12-3&n=5&d=10&s=1&s2=1&s3=1&s4=1&s5=1&cd=1&uuid=9965d&id=3L12-3&n=5&d=10&s=3&s2=1&s3=1&s4=1&s5=1&cd=1&uuid=9965d&id=3L12-3&n=20&d=10&s=11&s2=1&s3=1&s4=1&s5=1&cd=1&uuid=c4b73&id=3L12-2&n=10&d=10&s=8&s2=1&s3=2&s4=2&s5=true&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }