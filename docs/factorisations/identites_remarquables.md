---
author: Mireille COILHAC
title: Identités remarquables
tags:
  - calcul algébrique
---

Cliquer sur les solutions après avoir réfléchi ... 😊

<!---
???+ question "Mon exercice"

    **1.** Question 1.   

    Texte de la question indenté

    ??? success "Solution"

        Ici se trouve la solution rédigée de la question 1.
        Le texte doit être indenté dans cette nouvelle admonition

    **2.** Question 2.   
    Texte de la question indenté

    ??? success "Solution"

        Ici se trouve la solution rédigée de la question 2.
        Le texte doit être indenté dans cette nouvelle admonition

--->

??? note "Factoriser $a^2 + 2ab + b^2$"

    $(a+b)^2$

??? note "Factoriser $a^2 - 2ab + b^2$"

    $(a-b)^2$

??? note "Factoriser $a^2 - b^2$"

    $(a+b)(a-b)$


[Identités remarquables - 1](https://coopmaths.fr/alea/?uuid=0bd00&id=2N41-7a&alea=t7nk&i=1&uuid=874e8&id=2N41-7b&alea=dnRj&i=1&v=eleve&title=Factorisations+et+identit%C3%A9s+remarquablesns+quotients&es=011100){ .md-button target="_blank" rel="noopener" }


[Choisir factorisations 2 et niveau 2 : utiliser la flèche en haut à droite pour obtenir un nouvel exercice](https://www.mathix.org/exerciseur_calcul_litteral/#){ .md-button target="_blank" rel="noopener" }