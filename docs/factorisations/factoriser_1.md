---
author: Mireille COILHAC
title: Factorisation particulière
tags:
  - calcul algébrique
---

## I. Observons des exemples

!!! info "Observer des exemples"

    $$
    \begin{array}{l}\sqrt5+3\sqrt5=\sqrt5\left(\boxed1+3\right)=4\sqrt5\\\sqrt5+4x\sqrt5=\sqrt5\left(\boxed1+4x\right)\\\left(-x+2\right)+\left(-x+2\right)\left(7x-4\right)=\left(-x+2\right)\left[\boxed1+\left(7x-4\right)\right]=\left(-x+2\right)\left(7x-3\right)\\\left(-2x+3\right)\left(x-1\right)-2x+3=\left(-2x+3\right)\left(x-1+\boxed1\right)=\left(-2x+3\right)x\end{array}
    $$

## II. Exercices

???+ question "Exercice 1"

    **1.** Factoriser $A(x)=3x-2+\left(x+5\right)\left(6x-4\right)$

    ??? success "Solution"

        $$
        A(x)=3x-2+2\left(x+5\right)\left(3x-2\right)
        $$

        $$
        A(x)=\left(3x-2\right)\left(1+2\left(x+5\right)\right)
        $$

        $$
        A(x)=\boxed{\left(3x-2\right)\left(2x+11\right)}
        $$

    **2.** Factoriser $B(x)=5x-1-\left(5x-1\right)3x$

    ??? success "Solution"

        $$
        B(x)=5x-1-\left(5x-1\right)3x
        $$

        $$
        B(x)=\boxed{\left(5x-1\right)\left(1-3x\right)}
        $$

    **3.** Factoriser $C(x)=\left(3x-5\right)\left(3x+5\right)+3x-5$

    ??? success "Solution"

        $$
        C(x)=\left(3x-5\right)\left(3x+5\right)+3x-5
        $$

        $$
        C(x)=\left(3x-5\right)\left(\left(3x+5\right)+1\right)
        $$

        $$
        C(x)=\left(3x-5\right)\left(3x+6\right)
        $$ 

        $$
        C(x)=\boxed{3\left(3x-5\right)\left(x+2\right)}
        $$

    **4.** Factoriser $D(x)=x\left(x+1\right)+2\left(x^2-1\right)+x+1$

    ??? success "Solution"

        $$
        D(x)=x\left(x+1\right)+2\left(x^2-1\right)+x+1
        $$

        $$
        D(x)=x\left(x+1\right)+2\left(x-1\right)\left(x+1\right)+x+1
        $$

        $$
        D(x)=\left(x+1\right)\left[x+2\left(x-1\right)+1\right]
        $$

        $$
        D(x)=\boxed{\left(x+1\right)\left(3x-1\right)}
        $$

    **5.** Factoriser $E(x)=9x^2+12x+4+3x+2$

    ??? success "Solution"

        $$
        E(x)=\left(3x+2\right)^2+3x+2
        $$

        $$
        E(x)=\left(3x+2\right)\left[\left(3x+2\right)+1\right]
        $$

        $$
        E(x)=\left(3x+2\right)\left(3x+3\right)
        $$

        $$
        E(x)=\boxed{3\left(3x+2\right)\left(x+1\right)}
        $$


???+ question "Exercice 2"

    Refaire l'exercice suivant autant de fois que nécessaire. Il sera à chaque fois différent.

    Vous devez donner la forme la plus factorisée possible.

    <iframe src="https://euler-ressources.ac-versailles.fr/wims/wims.cgi?module=adm/raw&job=lightpopup&emod=H4/algebra/factorcom.fr&parm=cmd=new;exo=factorcom1;confparm1=2;confparm2=A;confparm2=5;confparm2=7;confparm3=1;confparm4=2;qnum=1;scoredelay=;seedrepeat=0;qcmlevel=1&option=noabout" width="100%" height="600"></iframe>