---
author: Mireille COILHAC
title: Tableaux de signes et inéquations
tags:
  - inéquations
---

## I. Les tableaux de signes

???+ question "Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=-3x+6$"

    **1.** Donner le sens de variation de $f$

    ??? success "Solution"

        Le coefficient directeur de la fonction affine $f$ est égal à $a=-3$.  
        $a<0$ donc la fonction $f$ est strictement décroissante sur $\mathbb{R}$.




    **2.** Représenter la fonction $f$ 

    ??? success "Solution"

        $f$ est représentée par une droite $D$.

        * $f(0)=6$ donc $D$ passe par $A(0; 6)$
        * $-3x+6=0$    
        $\Leftrightarrow -3x=-6$  
        $\Leftrightarrow x=2$   
        Donc $D$ passe par $B(2; 0)$

        ??? success "Solution" 

            ![d1](d1.png){ width=30% }

    **3.** Faire le tableau de signe de $f$

    ??? success "Solution" 

        En regardant la représentation graphique de la fonction $f$ on constate que :

        * Pour tout $x<2$ la représentation graphique est au-dessus de l'axe des abscisses (en vert) et donc $f(x)>0$.  
        👉 Pour ces $x$, il y aura donc **+** dans le tableau.
        * Pour $x=2$ la représentation graphique coupe l'axe des abscisses et donc $f(2)=0$.  
        👉 Pour  $x=2$, il y aura donc **0** dans le tableau.
        * Pour tout $x>2$ la représentation graphique est au-dessous de l'axe des abscisses (en rouge) et donc $f(x)<0$.  
        👉 Pour ces $x$, il y aura donc **-** dans le tableau.

        ??? success "Solution" 

            ![tab d1](tableau_d1.png){ width=50% }

    **4.** Résoudre "à la main" $-3x+6>0$

    Cela revient à chercher les réels $x$ pour lesquels il y aura **+** dans le tableau.

    ??? success "Solution" 

        $-3x+6>0$    
        $\Leftrightarrow -3x>-6$  
        $\Leftrightarrow x<\dfrac{-6}{-3}$  **👉 lorsqu'on multiplie ou divise les deux membres d'une inégalité par un réel négatif, on en change le sens.**     
        $\Leftrightarrow x<2$  

        Cela est bien cohérent avec le tableau de signe trouvé graphiquement.




???+ question "Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=3x+6$"

    **1.** Donner le sens de variation de $f$

    ??? success "Solution"

        Le coefficient directeur de la fonction affine $f$ est égal à $a=3$.  
        $a>0$ donc la fonction $f$ est strictement croissante sur $\mathbb{R}$.


    **2.** Représenter la fonction $f$ 

    ??? success "Solution"

        $f$ est représentée par une droite $D$.

        * $f(0)=6$ donc $D$ passe par $A(0; 6)$
        * $3x+6=0$    
        $\Leftrightarrow 3x=-6$  
        $\Leftrightarrow x=-2$   
        Donc $D$ passe par $B(-2; 0)$

        ??? success "Solution" 

            ![d1](d2.png){ width=30% }

    **3.** Faire le tableau de signe de $f$

    ??? success "Solution" 

        En regardant la représentation graphique de la fonction $f$ on constate que :

        * Pour tout $x<-2$ la représentation graphique est au-dessous de l'axe des abscisses (en rouge) et donc $f(x)<0$.  
        👉 Pour ces $x$, il y aura donc **-** dans le tableau.
        * Pour $x=-2$ la représentation graphique coupe l'axe des abscisses et donc $f(-2)=0$.  
        👉 Pour  $x=-2$, il y aura donc **0** dans le tableau.
        * Pour tout $x>-2$ la représentation graphique est au-dessus de l'axe des abscisses (en vert) et donc $f(x)>0$.  
        👉 Pour ces $x$, il y aura donc **+** dans le tableau.

        ??? success "Solution" 

            ![tab d1](tableau_d2.png){ width=50% }

    **4.** Résoudre "à la main" $3x+6>0$

    Cela revient à chercher les réels $x$ pour lesquels il y aura **+** dans le tableau.

    ??? success "Solution" 

        $3x+6>0$    
        $\Leftrightarrow 3x>-6$  
        $\Leftrightarrow x>\dfrac{-6}{3}$  **👉 lorsqu'on multiplie ou divise les deux membres d'une inégalité par un réel positif, on n'en change pas le sens.**     
        $\Leftrightarrow x>-2$  

        Cela est bien cohérent avec le tableau de signe trouvé graphiquement.


!!! info "A retenir"

    Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=ax+b$

    * Si $a>0$ alors $f$ est strictement croissante. Vous pouvez imaginer sa représentation graphique mentalement, et en déduire : 

    ![tableau théorique 2](tableau_2.png){ width=30% }


    * Si $a<0$ alors $f$ est strictement décroissante. Vous pouvez imaginer sa représentation graphique mentalement, et en déduire : 

    ![tableau théorique 1](tableau_1.png){ width=30% }



??? note pliée "lorsqu'on multiplie ou divise les deux membres d'une inégalité par un réel négatif on ... (cliquer)"

    **change** le sens de l'inégalité


??? note pliée "lorsqu'on multiplie ou divise les deux membres d'une inégalité par un réel positif on ... (cliquer)"

    **ne change pas** le sens de l'inégalité



## II. Entraînement à la résolution des inéquations : 

??? tip "Comment résoudre une inéquation ?"

    Il suffit de faire un tableau de signes, et de **lire** sur la première ligne les réels $x$ qui correspondent à ce que l'on cherche : 

    * **+** dans le tableau si l'on cherche $>0$
    * **-** dans le tableau si l'on cherche $<0$


!!! warning "Remarque"

    Les corrections proposées donnent des résolutions par calcul pour remplir les tableaux de signes. Vous pouvez les regarder, mais ce n'est pas nécessaire de le faire, car vous pouvez directement utiliser le sens de variation des fonctions affines pour remplir les + et - dans les tableaux de signes, comme nous l'avons vu ci-dessus.



[Inéquations produits](https://coopmaths.fr/alea/?uuid=014a4&id=2N61-2&n=4&d=10&s=6&alea=5HHm&i=1&cd=1&v=eleve&es=011100
){ .md-button target="_blank" rel="noopener" }

[Inéquations quotients](https://coopmaths.fr/alea/?uuid=0716b&id=2N61-4&alea=t1Nw&i=1&uuid=0716b&id=2N61-4&n=4&d=10&s=6&alea=KINg&i=1&cd=1&v=eleve&title=In%C3%A9quations+quotients&es=011100){ .md-button target="_blank" rel="noopener" }



